import { async, ComponentFixture, TestBed, getTestBed, tick, fakeAsync } from '@angular/core/testing';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, throwError } from 'rxjs';

describe('LoginComponent', () => {
    let injector: TestBed;
    let httpMock: HttpTestingController;
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                LoginComponent
            ],
            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
                FormsModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                FlexLayoutModule,
                MaterialModule,
            ],
            providers: [
                AppComponent
            ]
        })
            .compileComponents();
        injector = getTestBed();
        httpMock = injector.get(HttpTestingController);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    describe('When the ngOnInit() is called', () => {

        it('Should set a new title and reset the user label', fakeAsync(() => {
            spyOn<any>(component['titleService'], 'setTitle');
            spyOn(component, 'initFormControl');

            component['parent'].user = 'User 1';
            component['parent'].active = 'home';

            component.ngOnInit();
            tick(1000);
            expect(component['titleService'].setTitle).toHaveBeenCalledTimes(1);
            expect(component['titleService'].setTitle).toHaveBeenCalledWith('TUMRS - Login');
            expect(component.initFormControl).toHaveBeenCalledTimes(1);
            expect(component.initFormControl).toHaveBeenCalledWith();
            expect(component['parent'].user).toBeNull();
            expect(component['parent'].active).toEqual('list');
        }));
    });

    describe('When the initFormControl() is called', () => {

        it('Sould create the form with 2 fields', fakeAsync(() => {
            component.initFormControl();
            expect(component.form.get('username')).toBeDefined();
            expect(component.form.get('password')).toBeDefined();
        }));

        it('with filled fields Should create the form and should be valid', () => {
            component.initFormControl();
            component.form.get('username').setValue('user1');
            component.form.get('password').setValue('This15_4_53cr37');
            expect(component.form.valid).toBeTruthy();
        });
    });

    describe('When the disableInputs() is called', () => {

        it('Should set some variable and should disable the form', () => {
            component.initFormControl();
            component.loadingPage = false;
            component.messageError = 'Error';
            component.inputsBlocking = false;
            component.form.enable();
            component.buttonDisabled = false;
            component.disableInputs();
            expect(component.loadingPage).toBeTruthy();
            expect(component.messageError).toEqual('');
            expect(component.inputsBlocking).toBeTruthy();
            expect(component.form.disabled).toBeTruthy();
            expect(component.buttonDisabled).toBeTruthy();
        });
    });

    describe('When the enableInputs() is called', () => {

        it('Should set some variable and should enable the form', () => {
            component.initFormControl();
            component.loadingPage = true;
            component.inputsBlocking = true;
            component.form.disable();
            component.enableInputs();
            expect(component.loadingPage).toBeFalsy();
            expect(component.inputsBlocking).toBeFalsy();
            expect(component.form.disabled).toBeFalsy();
        });
    });

    describe('When the login() is called', () => {

        it('And the user is valid Should set the token and user variables', fakeAsync(() => {
            spyOn<any>(component['loginService'], 'login').and.returnValue(of({data: {token: 'token', user: {name: 'User 1'}}}));
            spyOn<any>(component['router'], 'navigate');
            spyOn<any>(component['storageManager'], 'set').and.returnValue(null);
            spyOn<any>(component['tokenManager'], 'set').and.returnValue(null);

            component.initFormControl();
            component.form.get('username').setValue('user1');
            component.form.get('password').setValue('This15_4_53cr37');
            component.login();

            tick(1000);

            expect(component['parent'].user).toEqual('User 1');
            expect(component['storageManager'].set).toHaveBeenCalledTimes(1);
            expect(component['storageManager'].set).toHaveBeenCalledWith('user', {name: 'User 1'});
            expect(component['tokenManager'].set).toHaveBeenCalledTimes(1);
            expect(component['tokenManager'].set).toHaveBeenCalledWith('token');
            expect(component['router'].navigate).toHaveBeenCalledTimes(1);
            expect(component['router'].navigate).toHaveBeenCalledWith(['movies']);

        }));

        it('And the response is 200 but payload not present Should do nothing', () => {
            spyOn(component, 'disableInputs');
            spyOn<any>(component['router'], 'navigate');
            spyOn<any>(component['storageManager'], 'set').and.returnValue(null);
            spyOn<any>(component['tokenManager'], 'set').and.returnValue(null);
            spyOn<any>(component['loginService'], 'login').and.returnValue(of({data: { }}));

            component.initFormControl();
            component.form.get('username').setValue('user1');
            component.form.get('password').setValue('This15_4_53cr37');
            component.login();

            expect(component.disableInputs).toHaveBeenCalledTimes(1);
            expect(component['storageManager'].set).toHaveBeenCalledTimes(0);
            expect(component['tokenManager'].set).toHaveBeenCalledTimes(0);
            expect(component['router'].navigate).toHaveBeenCalledTimes(0);
        });

        it('And the response is 200 but payload not present Should do nothing', () => {
            spyOn(component, 'disableInputs');
            spyOn<any>(component['router'], 'navigate');
            spyOn<any>(component['storageManager'], 'set').and.returnValue(null);
            spyOn<any>(component['tokenManager'], 'set').and.returnValue(null);

            spyOn<any>(component['loginService'], 'login').and.returnValue(of());

            component.initFormControl();
            component.form.get('username').setValue('');
            component.form.get('password').setValue('This15_4_53cr37');
            component.login();

            expect(component.disableInputs).toHaveBeenCalledTimes(0);
            expect(component['storageManager'].set).toHaveBeenCalledTimes(0);
            expect(component['tokenManager'].set).toHaveBeenCalledTimes(0);
            expect(component['router'].navigate).toHaveBeenCalledTimes(0);
        });

        it('And error 500 is returned Should show a error message', () => {
            spyOn(component, 'enableInputs');
            spyOn<any>(component['router'], 'navigate');
            spyOn<any>(component['loginService'], 'login').and.returnValue(throwError({status: 500}));

            component.initFormControl();
            component.form.get('username').setValue('user1');
            component.form.get('password').setValue('This15_4_53cr37');
            component.login();

            expect(component.enableInputs).toHaveBeenCalledTimes(1);
            expect(component['router'].navigate).toHaveBeenCalledTimes(0);
            expect(component.messageError).toEqual('An internal error.');
        });

        it('And error 401 is returned Should show a error message', () => {
            spyOn(component, 'enableInputs');
            spyOn<any>(component['router'], 'navigate');
            spyOn<any>(component['loginService'], 'login').and.returnValue(throwError({status: 401}));

            component.initFormControl();
            component.form.get('username').setValue('user1');
            component.form.get('password').setValue('This15_4_53cr37');
            component.login();

            expect(component.enableInputs).toHaveBeenCalledTimes(1);
            expect(component['router'].navigate).toHaveBeenCalledTimes(0);
            expect(component.messageError).toEqual('Unauthorized user.');
        });
    });
});
