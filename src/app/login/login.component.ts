import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { StorageManagerService } from '../services/storage-manager.service';
import { TokenManagerService } from '../services/token-manager.service';
import { AppComponent } from '../app.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    form: FormGroup;
    loadingPage = false;
    messageError = '';
    inputsBlocking = false;
    buttonDisabled = false;

    constructor(
        private loginService: LoginService,
        private storageManager: StorageManagerService,
        private tokenManager: TokenManagerService,
        private router: Router,
        private titleService: Title,
        private parent: AppComponent,
    ) { }

    ngOnInit() {
        this.titleService.setTitle('TUMRS - Login');
        setTimeout(() => {
            this.parent.user = null;
            this.parent.active = 'list';
        });
        this.initFormControl();
    }

    initFormControl() {
        this.form = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        });
    }

    disableInputs() {
        this.loadingPage = true;
        this.messageError = '';
        this.inputsBlocking = true;
        this.form.disable();
        this.buttonDisabled = true;
    }

    enableInputs() {
        this.loadingPage = false;
        this.inputsBlocking = false;
        this.form.enable();
    }

    login() {
        if (this.form.valid) {
            this.disableInputs();
            this.loginService.login(this.form.value).subscribe(
                (response) => {
                    if (response.data && response.data.token) {

                        // Updates the user name in the layout
                        setTimeout(() => {
                            this.parent.user = response.data.user.name;
                        });
                        // saves user info into the localStorage
                        this.storageManager.set('user', response.data.user);

                        // saves the token into the localStorage
                        this.tokenManager.set(response.data.token);

                        // redirect to movie list page
                        this.router.navigate(['movies']);
                    }
                },
                (error) => {
                    this.enableInputs();
                    console.error('An internal error: ', error);
                    if (error && error.status && error.status === 401) {
                        this.messageError = 'Unauthorized user.';
                    } else {
                        this.messageError = 'An internal error.';
                    }
                }
            );
        }
    }

}
