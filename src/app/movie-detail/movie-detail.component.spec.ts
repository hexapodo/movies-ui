import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieDetailComponent } from './movie-detail.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LoaderComponent } from '../loader/loader.component';
import { MovieInfoComponent } from '../movie-info/movie-info.component';
import { MyRankComponent } from '../my-rank/my-rank.component';
import { CommunityComponent } from '../community/community.component';
import { RatingModule } from 'ng-starrating';
import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('MovieDetailComponent', () => {
    let injector: TestBed;
    let httpMock: HttpTestingController;
    let component: MovieDetailComponent;
    let fixture: ComponentFixture<MovieDetailComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MovieDetailComponent,
                LoaderComponent,
                MovieInfoComponent,
                MyRankComponent,
                CommunityComponent,
            ],
            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                RatingModule,
                FlexLayoutModule,
                MaterialModule
            ],
            providers: [
                AppComponent
            ]
        })
            .compileComponents();
        injector = getTestBed();
        httpMock = injector.get(HttpTestingController);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
