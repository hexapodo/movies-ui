import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AppComponent } from '../app.component';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { MovieService } from '../services/movie.service';
import { RankService } from '../services/rank.service';

import { Subscription } from 'rxjs';
import { MyRankComponent } from '../my-rank/my-rank.component';
import { Movie } from '../types/movie.type';
import { Rank } from '../types/rank-interface';

@Component({
    selector: 'app-movie-detail',
    templateUrl: './movie-detail.component.html',
    styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit, OnDestroy {

    @ViewChild(MyRankComponent, { static: true }) myRankComponent: MyRankComponent;

    loading = false;
    parameters: Subscription = null;
    notificatorOptions: MatSnackBarConfig = {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 5000
    };
    messages = {
        add: 'Ranking was saved',
        modify: 'Ranking was modified',
        delete: 'Ranking was deleted',
        error: 'It was an error, try later'
    };
    movie: Movie = Movie.import({});

    constructor(
        private parent: AppComponent,
        private route: ActivatedRoute,
        private movieService: MovieService,
        private rankService: RankService,
        private titleService: Title,
        private notificator: MatSnackBar
    ) { }

    ngOnInit() {
        this.titleService.setTitle('TUMRS - Movie details');
        setTimeout(() => {
            this.parent.active = 'detail';
        });
        this.parameters = this.route.params.subscribe(
            (params: Params) => {
                this.getMovie(params.movieId);
            }
        );
    }

    getMovie(movieId) {
        this.loading = true;
        this.movieService.getById(movieId).subscribe(
            (response) => {
                this.loading = false;
                this.movie = Movie.import(response.body);
            },
            (error) => {
                this.loading = false;
                console.error(error);
            }
        );
    }

    ngOnDestroy(): void {
        if (this.parameters) {
            this.parameters.unsubscribe();
        }
    }

    saveMyRank(event: Rank) {
        if (event.id) {
            this.rankService.modify(event.id, event).subscribe(
                (response) => {
                    this.notify(this.messages.modify);
                },
                (error) => {
                    this.notify(this.messages.error);
                    console.error(error);
                }
            );
        } else {
            const data: any = { ... event };
            data.movieId = this.movie.id;
            this.rankService.add(data).subscribe(
                (response) => {
                    this.notify(this.messages.add);
                    this.myRankComponent.id = response.rankId;
                    this.myRankComponent.updateId(response.body.rankId);
                },
                (error) => {
                    this.notify(this.messages.error);
                    console.error(error);
                }
            );
        }
    }

    resetRank() {
        this.rankService.delete(this.movie.id).subscribe(
            (response) => {
                this.notify(this.messages.delete);
            },
            (error) => {
                this.notify(this.messages.error);
                console.error(error);
            }
        );
    }

    notify(msg) {
        this.notificator.open(msg, null, this.notificatorOptions);
    }

}
