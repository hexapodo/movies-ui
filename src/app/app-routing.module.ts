import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { LoginComponent } from './login/login.component';
import { GuardService } from './services/guard.service';
import { GuardLoginService } from './services/guard.login.service';

const routes: Routes = [
    { path: '', redirectTo: 'movies', pathMatch: 'full' },
    { path: 'login', component: LoginComponent, canActivate: [ GuardLoginService ] },
    { path: 'movies', component: MovieListComponent, canActivate: [ GuardService ] },
    { path: 'detail/:movieId', component: MovieDetailComponent, canActivate: [ GuardService ] },
    { path: '**', redirectTo: 'movies', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
