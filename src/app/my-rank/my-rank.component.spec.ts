import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyRankComponent } from './my-rank.component';
import { RatingModule } from 'ng-starrating';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('MyRankComponent', () => {
    let component: MyRankComponent;
    let fixture: ComponentFixture<MyRankComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MyRankComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                RatingModule,
                BrowserAnimationsModule,
                FlexLayoutModule,
                MaterialModule,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MyRankComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
