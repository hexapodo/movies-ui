import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Rank } from '../types/rank-interface';

@Component({
    selector: 'app-my-rank',
    templateUrl: './my-rank.component.html',
    styleUrls: ['./my-rank.component.scss']
})
export class MyRankComponent implements OnInit, OnChanges {

    @Input() id = null;
    @Input() rate = 0;
    @Input() comment = '';
    @Output() save: EventEmitter<Rank> = new EventEmitter();
    @Output() resetRank: EventEmitter<void> = new EventEmitter();

    form: FormGroup;

    constructor() { }

    ngOnInit() {
        this.initFormControl();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.form) {
            this.form.get('id').setValue(this.id);
            this.form.get('rank').setValue(this.rate);
            this.form.get('comment').setValue(this.comment);
        }
    }

    initFormControl() {
        this.form = new FormGroup({
            id: new FormControl(null),
            comment: new FormControl('', Validators.required),
            rank: new FormControl(0, [Validators.min(1), Validators.max(5)])
        });
    }

    onRate(event: any) {
        this.rate = event.newValue;
        this.form.get('rank').setValue(this.rate);
    }

    reset() {
        this.rate = 0;
        this.comment = '';
        this.form.get('id').setValue(null);
        this.form.get('rank').setValue(this.rate);
        this.form.get('comment').setValue(this.comment);
        this.resetRank.emit();
    }

    onSave() {
        this.save.emit(this.form.value);
    }

    updateId(id) {
        this.form.get('id').setValue(id);
    }

}
