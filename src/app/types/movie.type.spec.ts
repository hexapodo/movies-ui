import { Movie } from './movie.type';
import { Rank } from './rank.type';

describe('Movie Class', () => {
    it('Should create', () => {
        expect(new Movie()).toBeDefined();
    });

    it('Should create a default object', () => {
        const movie = Movie.import({ });
        expect(movie).toEqual(jasmine.any(Movie));
        expect(movie.id).toBeNull();
        expect(movie.title).toBeNull();
        expect(movie.date).toBeNull();
        expect(movie.rank).toBeNull();
        expect(movie.myRank).toBeNull();
        expect(movie.community).toEqual([]);
        expect(movie.poster).toBeNull();
        expect(movie.description).toBeNull();
    });

    it('Should create a default object', () => {
        const movie = Movie.import({ community: [{}], myRank: {}});
        expect(movie).toEqual(jasmine.any(Movie));
        expect(movie.id).toBeNull();
        expect(movie.title).toBeNull();
        expect(movie.date).toBeNull();
        expect(movie.rank).toBeNull();
        expect(movie.myRank).toEqual(jasmine.any(Rank));
        expect(movie.community).toEqual(jasmine.any(Array));
        expect(movie.community[0]).toEqual(jasmine.any(Rank));
        expect(movie.poster).toBeNull();
        expect(movie.description).toBeNull();
    });

    it('Should create an array of default object', () => {
        const movies = Movie.importArray([{}]);
        expect(movies).toEqual(jasmine.any(Array));
        expect(movies[0].id).toBeNull();
        expect(movies[0].title).toBeNull();
        expect(movies[0].date).toBeNull();
        expect(movies[0].rank).toBeNull();
        expect(movies[0].myRank).toBeNull();
        expect(movies[0].community).toEqual([]);
        expect(movies[0].poster).toBeNull();
        expect(movies[0].description).toBeNull();
    });

    it('Should create a custom object', () => {
        const userMock = {
            id: 2,
            title: 'title',
            date: 'date',
            rank: 3,
            poster: 'poster',
            description: 'description'
        };
        const movie = Movie.import(userMock);
        expect(movie).toEqual(jasmine.any(Movie));
        expect(movie.id).toEqual(jasmine.any(Number));
        expect(movie.title).toEqual(jasmine.any(String));
        expect(movie.date).toEqual(jasmine.any(String));
        expect(movie.rank).toEqual(jasmine.any(Number));
        expect(movie.poster).toEqual(jasmine.any(String));
        expect(movie.description).toEqual(jasmine.any(String));
        expect(movie.myRank).toBeNull();
        expect(movie.community).toEqual(jasmine.any(Array));
        expect(movie.id).toEqual(2);
        expect(movie.title).toEqual('title');
        expect(movie.date).toEqual('date');
        expect(movie.rank).toEqual(3);
        expect(movie.poster).toEqual('poster');
        expect(movie.description).toEqual('description');
        expect(movie.myRank).toBeNull();
        expect(movie.community).toEqual([]);
    });
});
