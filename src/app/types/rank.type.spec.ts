import { Rank } from './rank.type';

describe('Rank Class', () => {
    it('Should create', () => {
        expect(new Rank()).toBeDefined();
    });

    it('Should create a default object', () => {
        const rank = Rank.import({ });
        expect(rank).toEqual(jasmine.any(Rank));
        expect(rank.id).toBeNull();
        expect(rank.rank).toBeNull();
        expect(rank.comment).toBeNull();
        expect(rank.user).toBeNull();
    });

    it('Should create a custom object', () => {
        const rankMock = { id: 2, rank: 3, comment: 'this is the comment' };
        const rank = Rank.import(rankMock);
        expect(rank).toEqual(jasmine.any(Rank));
        expect(rank.id).toEqual(jasmine.any(Number));
        expect(rank.rank).toEqual(jasmine.any(Number));
        expect(rank.comment).toEqual(jasmine.any(String));
        expect(rank.id).toEqual(2);
        expect(rank.rank).toEqual(3);
        expect(rank.comment).toEqual('this is the comment');
        expect(rank.user).toBeNull();
    });
});
