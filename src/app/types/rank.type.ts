import { User } from './user.type';

export class Rank {
    public id: number;
    public rank: number;
    public comment: string;
    public user: User;

    constructor() {}

    public static import(dataRaw: any): Rank {

        const item = new Rank();

        item.id = dataRaw.hasOwnProperty('id') ? dataRaw.id : null;
        item.rank = dataRaw.hasOwnProperty('rank') ? dataRaw.rank : null;
        item.comment = dataRaw.hasOwnProperty('comment') ? dataRaw.comment : null;
        item.user = dataRaw.hasOwnProperty('user') ? User.import(dataRaw.user) : null;
        return item;
    }
}
