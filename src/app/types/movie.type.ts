import { Rank } from './rank.type';

export class Movie {
    public id: number;
    public title: string;
    public date: string;
    public rank: number;
    public poster: string;
    public description: string;
    public myRank: Rank;
    public community: Rank[] = [];

    constructor() {}

    public static import(dataRaw: any): Movie {

        const item = new Movie();

        item.id = dataRaw.hasOwnProperty('id') ? dataRaw.id : null;
        item.title = dataRaw.hasOwnProperty('title') ? dataRaw.title : null;
        item.date = dataRaw.hasOwnProperty('date') ? dataRaw.date : null;
        item.rank = dataRaw.hasOwnProperty('rank') ? dataRaw.rank : null;
        item.poster = dataRaw.hasOwnProperty('poster') ? dataRaw.poster : null;
        item.description = dataRaw.hasOwnProperty('description') ? dataRaw.description : null;
        item.myRank = dataRaw.hasOwnProperty('myRank') ? Rank.import(dataRaw.myRank) : null;

        if (dataRaw.hasOwnProperty('community')) {
            for (const itemRaw of dataRaw.community) {
                const element = Rank.import(itemRaw);
                item.community.push(element);
            }
        } else {
            item.community = [];
        }
        return item;
    }

    public static importArray(dataRaw: any): Movie[] {
        let movies: Movie[];
        movies = [];
        for (const itemRaw of dataRaw) {
            const element = Movie.import(itemRaw);
            movies.push(element);
        }
        return movies;
    }

}
