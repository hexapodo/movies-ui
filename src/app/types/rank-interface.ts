
interface User {
    id?: number;
    name: string;
    username: string;
}

export interface Rank {
    id: number;
    rank: number;
    comment: string;
    user?: User;
}
