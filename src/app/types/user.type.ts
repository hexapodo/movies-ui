
export class User {
    public id: number;
    public name: string;
    public username: string;

    constructor() {}

    public static import(dataRaw: any): User {

        const item = new User();

        item.id = dataRaw.hasOwnProperty('id') ? dataRaw.id : null;
        item.name = dataRaw.hasOwnProperty('name') ? dataRaw.name : null;
        item.username = dataRaw.hasOwnProperty('username') ? dataRaw.username : null;
        return item;
    }
}
