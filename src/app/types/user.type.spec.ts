import { User } from './user.type';

describe('User Class', () => {
    it('Should create', () => {
        expect(new User()).toBeDefined();
    });

    it('Should create a default object', () => {
        const user = User.import({ });
        expect(user).toEqual(jasmine.any(User));
        expect(user.id).toBeNull();
        expect(user.name).toBeNull();
        expect(user.username).toBeNull();
    });

    it('Should create a custom object', () => {
        const userMock = { id: 2, name: 'User 1', username: 'user1' };
        const user = User.import(userMock);
        expect(user).toEqual(jasmine.any(User));
        expect(user.id).toEqual(jasmine.any(Number));
        expect(user.name).toEqual(jasmine.any(String));
        expect(user.username).toEqual(jasmine.any(String));
        expect(user.id).toEqual(2);
        expect(user.name).toEqual('User 1');
        expect(user.username).toEqual('user1');
    });
});
