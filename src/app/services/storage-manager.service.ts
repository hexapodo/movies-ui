import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StorageManagerService {

    constructor() { }

    set(key: string, object: object | string) {
        return localStorage.setItem(key, JSON.stringify(object));
    }

    get(key: string) {
        return JSON.parse(localStorage.getItem(key));
    }

    destroy(key: string) {
        return localStorage.removeItem(key);
    }

    length(key: string) {
        return this.get(key).length;
    }

}
