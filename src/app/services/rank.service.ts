import { Injectable } from '@angular/core';
import { BackendConnection, BackendServiceInterface } from './backend-connection';
import { HttpClient } from '@angular/common/http';
import { TokenManagerService } from './token-manager.service';

@Injectable({
    providedIn: 'root'
})
export class RankService extends BackendConnection implements BackendServiceInterface {
    endpoint = 'api/rank';

    constructor(
        protected http: HttpClient,
        protected tokenManagerService: TokenManagerService
    ) {
        super(http, tokenManagerService);
    }

    delete(movieId) {
        return super.delete(this.endpoint, movieId);
    }

    modify(rankId, data) {
        return super.put(this.endpoint, rankId, data);
    }

    add(newData) {
        return super.post(this.endpoint, newData);
    }

}
