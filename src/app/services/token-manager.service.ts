import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenManagerService {

  constructor() { }

  get(): string {
    return localStorage.getItem('X-Movies-Token');
  }

  set(value: string): void {
    localStorage.setItem('X-Movies-Token', value);
  }

  destroy(): void {
    localStorage.removeItem('X-Movies-Token');
  }
}
