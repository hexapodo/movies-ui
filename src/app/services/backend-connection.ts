import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenManagerService } from './token-manager.service';
import { environment } from '../../environments/environment';


export class BackendConnection {

    url = environment.backendUrl;

    private httpHeaders: HttpHeaders = new HttpHeaders();

    constructor(
        protected http: HttpClient,
        protected tokenManagerService: TokenManagerService
    ) { }

    getAll(endpoint: string): Observable<any> {
        const updatedHeaders: HttpHeaders = this.updateHeaders();
        return this.http.get<any>(this.url + endpoint, { observe: 'response', headers: updatedHeaders });
    }

    getOne(endpoint: string, id: string): Observable<any> {
        const updatedHeaders: HttpHeaders = this.updateHeaders();
        return this.http.get<any>(this.url + endpoint + '/' + id, { observe: 'response', headers: updatedHeaders });
    }

    post(endpoint: string, payload: object): Observable<any> {
        const updatedHeaders: HttpHeaders = this.updateHeaders();
        return this.http.post<any>(this.url + endpoint, payload, { observe: 'response', headers: updatedHeaders });
    }

    put(endpoint: string, id: string, payload: object): Observable<any> {
        const updatedHeaders: HttpHeaders = this.updateHeaders();
        return this.http.put<any>(this.url + endpoint + '/' + id, payload, { observe: 'response', headers: updatedHeaders });
    }

    delete(endpoint: string, entity: string): Observable<any> {
        const updatedHeaders: HttpHeaders = this.updateHeaders();
        return this.http.delete<any>(this.url + endpoint + '/' + entity, { observe: 'response', headers: updatedHeaders });
    }

    private updateHeaders(type: string = 'json'): HttpHeaders {
        const token: string = this.tokenManagerService.get();
        let headers: HttpHeaders = this.httpHeaders;
        switch (type) {
            case 'json':
                headers = this.httpHeaders.append('Content-Type', 'application/json');
                break;
            default:
                break;
        }
        if (token) {
            headers = this.httpHeaders.append('X-Movies-Token', token);
        }
        return headers;
    }
}

export interface BackendServiceInterface {
    url: string;
    endpoint: string;
}
