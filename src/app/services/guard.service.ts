import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { TokenManagerService } from './token-manager.service';

@Injectable({
    providedIn: 'root'
})
export class GuardService implements CanActivate {

    constructor(
        private tokenManegerService: TokenManagerService,
        public router: Router
    ) { }

    canActivate() {
        const token = this.tokenManegerService.get();
        if (!token) {
            this.router.navigate(['login']);
        }
        return token === null ? false : true;
    }
}
