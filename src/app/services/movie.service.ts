import { Injectable } from '@angular/core';
import { BackendConnection, BackendServiceInterface } from './backend-connection';
import { HttpClient } from '@angular/common/http';
import { TokenManagerService } from './token-manager.service';

@Injectable({
    providedIn: 'root'
})
export class MovieService extends BackendConnection implements BackendServiceInterface {
    endpoint = 'api/movie';

    constructor(
        protected http: HttpClient,
        protected tokenManagerService: TokenManagerService
    ) {
        super(http, tokenManagerService);
    }

    getByYear(year: string | number, page: number) {
        return super.getAll(this.endpoint + '/year/' + year + '?page=' + page);
    }

    getById(movieId: number) {
        return super.getAll(this.endpoint + '/' + movieId );
    }

}
