import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BackendConnection, BackendServiceInterface } from './backend-connection';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { TokenManagerService } from './token-manager.service';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoginService extends BackendConnection implements BackendServiceInterface {

    endpoint = 'api/login';

    private loginSubject = new BehaviorSubject(null);
    private info: any;

    constructor(
        protected http: HttpClient,
        protected tokenManagerService: TokenManagerService
    ) {
        super(http, tokenManagerService);
    }

    onLogin(): Observable<any> {
        return this.loginSubject.asObservable();
    }

    private refresh() {
        this.loginSubject.next(this.info);
    }

    login(payload: any): Observable<any> {
        return super.post(this.endpoint, payload)
            .pipe(
                map((data: HttpResponse<any>) => {
                    this.info = data.body.info;
                    this.refresh();
                    return {
                        data: {
                            token: data.body.hasOwnProperty('token') ? data.body.token : null,
                            user: data.body.hasOwnProperty('user') ? data.body.user : null
                        },
                        status: data.status
                    };
                })
            );
    }

}
