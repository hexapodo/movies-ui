import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                FlexLayoutModule
            ],
            declarations: [
                AppComponent
            ],
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    describe('When the ngOnInit() is called & storage has a valid user', () => {

        it('should call to storageManager.get()', () => {
            const fixture = TestBed.createComponent(AppComponent);
            const app = fixture.debugElement.componentInstance;
            spyOn<any>(app['storageManager'], 'get').and.returnValue({ name: 'User 1'});
            app.ngOnInit();
            expect(app['storageManager'].get).toHaveBeenCalledTimes(3);
            expect(app['storageManager'].get).toHaveBeenCalledWith('user');
        });

        it('should call to storageManager.get()', () => {
            const fixture = TestBed.createComponent(AppComponent);
            const app = fixture.debugElement.componentInstance;
            spyOn<any>(app['storageManager'], 'get').and.returnValue(null);
            app.ngOnInit();
            expect(app['storageManager'].get).toHaveBeenCalledTimes(1);
            expect(app['storageManager'].get).toHaveBeenCalledWith('user');
        });
    });

    describe('When the goToList() is called', () => {

        it('should call to router.navigate()', () => {
            const fixture = TestBed.createComponent(AppComponent);
            const app = fixture.debugElement.componentInstance;
            spyOn<any>(app['router'], 'navigate');
            app.goToList();
            expect(app['router'].navigate).toHaveBeenCalledTimes(1);
            expect(app['router'].navigate).toHaveBeenCalledWith(['movies']);
        });
    });

    describe('When the logout() is called & storage has a valid user', () => {

        it('should destroy the session & redirect to login page', () => {
            const fixture = TestBed.createComponent(AppComponent);
            const app = fixture.debugElement.componentInstance;
            spyOn<any>(app['storageManager'], 'destroy');
            spyOn<any>(app['tokenManager'], 'destroy');
            spyOn<any>(app['router'], 'navigate');
            app.logout();
            expect(app['tokenManager'].destroy).toHaveBeenCalledTimes(1);
            expect(app['tokenManager'].destroy).toHaveBeenCalledWith();
            expect(app['storageManager'].destroy).toHaveBeenCalledTimes(1);
            expect(app['storageManager'].destroy).toHaveBeenCalledWith('user');
            expect(app['router'].navigate).toHaveBeenCalledTimes(1);
            expect(app['router'].navigate).toHaveBeenCalledWith(['login']);
        });

    });

});
