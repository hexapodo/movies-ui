import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../types/movie.type';

@Component({
    selector: 'app-movie-info',
    templateUrl: './movie-info.component.html',
    styleUrls: ['./movie-info.component.scss']
})
export class MovieInfoComponent implements OnInit {

    @Input() movie: Movie = Movie.import({});

    constructor() { }

    ngOnInit() {
    }

}
