import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../types/movie.type';

@Component({
    selector: 'app-movie-card',
    templateUrl: './movie-card.component.html',
    styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {

    @Input() movie: Movie = Movie.import({});

    constructor() { }

    ngOnInit() {
    }

}
