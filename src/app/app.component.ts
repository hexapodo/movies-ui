import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageManagerService } from './services/storage-manager.service';
import { TokenManagerService } from './services/token-manager.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    active = 'list';
    user: string = null;

    constructor(
        private router: Router,
        private storageManager: StorageManagerService,
        private tokenManager: TokenManagerService
    ) { }

    ngOnInit(): void {
        this.user = this.storageManager.get('user') && this.storageManager.get('user').name ? this.storageManager.get('user').name : null;
    }

    goToList(): void {
        this.router.navigate(['movies']);
    }

    logout(): void {
        this.storageManager.destroy('user');
        this.tokenManager.destroy();
        this.router.navigate(['login']);
    }

}
