import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PageEvent } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { MovieService } from '../services/movie.service';
import { Movie } from '../types/movie.type';

@Component({
    selector: 'app-movie-list',
    templateUrl: './movie-list.component.html',
    styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

    loading = false;
    years: number[] = Array(10).fill(0).map((_, i) => {
        return i + 2010;
    });
    selectedYear = 2019;
    length = 0;
    pageSize = 20;
    pageEvent: PageEvent = {
        pageIndex: 0,
        pageSize: 0,
        length: 0
    };
    movies: Movie[] = [];

    constructor(
        private router: Router,
        private parent: AppComponent,
        private movieService: MovieService,
        private titleService: Title
    ) { }

    ngOnInit() {
        this.titleService.setTitle('TUMRS - List of movies');
        setTimeout(() => {
            this.parent.active = 'list';
        });
        this.getMovies('2019');
    }

    getMovies(year: string | number, page: number = 1) {
        this.loading = true;
        this.movieService.getByYear(year, page).subscribe(
            (response) => {
                this.loading = false;
                this.length = response.body.total_results;
                this.movies = Movie.importArray(response.body.results);
            },
            (error) => {
                this.loading = false;
                console.error(error);
            }
        );
    }

    getPage(event: PageEvent) {
        this.pageEvent = event;
        this.getMovies(this.selectedYear, this.pageEvent.pageIndex + 1);
    }

    onSelectYear(item: MatSelectChange) {
        this.selectedYear = item.value;
        this.getMovies(this.selectedYear);
        this.pageEvent.pageIndex = 0;
    }

    goToDetail(movieId) {
        this.router.navigate(['detail', movieId]);
    }

}
