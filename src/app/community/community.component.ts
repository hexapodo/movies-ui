import { Component, OnInit, Input } from '@angular/core';
import { Rank } from '../types/rank.type';

@Component({
    selector: 'app-community',
    templateUrl: './community.component.html',
    styleUrls: ['./community.component.scss']
})
export class CommunityComponent implements OnInit {

    @Input() communityRanks: Rank[] = [];

    constructor() { }

    ngOnInit() {
    }

}
